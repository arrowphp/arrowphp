<?php

declare(strict_types=1);

namespace Arrow\Test\Functional\Middleware;

use Arrow\Config;
use Arrow\Test\AppTestCase;
use League\Event\BufferedEventDispatcher;

/**
 * @group Functional
 * @group ModuleLoader
 */
class ModuleLoaderTest extends AppTestCase {

	public function testLoadModuleWithMissingModuleClassDoesNotThrowException(): void {
		$this->expectNotToPerformAssertions();

		$this->app->get(Config::class)->set('Module.ClassNamePattern', '\{Namespace}\{Name}\XXX{Name}Module');

		$this->app->get(BufferedEventDispatcher::class)->emit(new \Arrow\Event\ApplicationBootEvent($this->app));
	}

	public function testLoadModuleWithInvalidModuleClassThrowsException(): void {
		$this->expectExceptionMessage('Invalid.');

		$this->app->get(Config::class)->set('Module.ClassNamePattern', '\{Namespace}\{Name}\{Name}ModuleInvalid');

		$this->app->get(BufferedEventDispatcher::class)->emit(new \Arrow\Event\ApplicationBootEvent($this->app));
	}

	public function testLoadModule(): void {
		$this->expectNotToPerformAssertions();

		$this->app->get(Config::class)->set('Module.ClassNamePattern', '\{Namespace}\{Name}\{Name}');

		$this->app->get(BufferedEventDispatcher::class)->emit(new \Arrow\Event\ApplicationBootEvent($this->app));
	}
}
