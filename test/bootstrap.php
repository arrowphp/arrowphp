<?php declare(strict_types = 1);

// phpcs:ignoreFile

require_once 'vendor/autoload.php';

define('ARROW_PATH_SELF', realpath(__DIR__ . '/..'));
define('ARROW_PATH_VENDOR', realpath(__DIR__ . '/../vendor'));
define('ARROW_PATH_BASE', realpath(__DIR__ . '/../test-app'));
