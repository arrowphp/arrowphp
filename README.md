# Arrow

[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-buildstatus]][link-pipelines]
[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
<!-- [![Coverage Status][ico-scrutinizer]][link-scrutinizer] -->
<!-- [![Quality Score][ico-code-quality]][link-code-quality] -->

Introducing Arrow! The no fuss framework built on the components of a highly respected collection of moderated components held to a high standard.

The name and it’s purpose comes from the php arrow "->".

## Pre-Release - NOTES / TODO

1. Review all code to ensure return type consistency
1. Updates tests
  - Add hints to indicate files to skip in code coverage
1. Investigate/Implement PHAN!
1. Split out build
  - Init setup skeleton project

## Install

Via Composer

Via project composer.json

``` json
  "require": {
    "arrowphp/core": "@dev"
  }
```

## Usage

Example index.php file (using Guzzle for Psr7)

``` php
require_once __DIR__.'/../vendor/autoload.php';

$app = new \Arrow\Application();

$request = \GuzzleHttp\Psr7\ServerRequest::fromGlobals();
$response = new \GuzzleHttp\Psr7\Response();

$response = $app->run($request, $response);
$app->flush($response);
$app->terminate();
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer run-ci
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please contact [Chris Pennycuick][link-author] directly instead of using the issue tracker.

## Credits

- [Chris Pennycuick][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/arrowphp/arrow.svg
[ico-downloads]: https://img.shields.io/packagist/dt/arrowphp/arrow.sv
[ico-buildstatus]: https://gitlab.com/arrowphp/arrow/badges/master/build.svg
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg
<!-- [ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/:vendor/:package_name.svg -->
<!-- [ico-code-quality]: https://img.shields.io/scrutinizer/g/:vendor/:package_name.svg -->

[link-packagist]: https://packagist.org/packages/arrowphp/arrow
[link-downloads]: https://packagist.org/packages/arrowphp/arrow
[link-author]: https://gitlab.com/christopher.pennycuick
[link-contributors]: https://gitlab.com/arrowphp/arrow/graphs/master
[link-pipelines]: https://gitlab.com/arrowphp/arrow/pipelines
<!-- [link-scrutinizer]: https://scrutinizer-ci.com/g/:vendor/:package_name/code-structure -->
<!-- [link-code-quality]: https://scrutinizer-ci.com/g/:vendor/:package_name -->
