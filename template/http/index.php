<?php declare(strict_types=1);

use Arrow\Application;
use GuzzleHttp\Psr7\{ServerRequest, Response};
use Psr\Http\Message\{RequestInterface, ResponseInterface};

require_once __DIR__.'/../vendor/autoload.php';

$app = new Application((bool)getenv('ARROW_CACHE_ENABLED'));

$app->add(RequestInterface::class, fn() => ServerRequest::fromGlobals());
$app->add(ResponseInterface::class, Response::class);

$request = $this->get(RequestInterface::class);
$response = $app->run($request);
$app->flush($response);
