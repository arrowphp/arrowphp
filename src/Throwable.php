<?php

declare(strict_types=1);

namespace Arrow;

interface Throwable extends \Throwable {

	/**
	 * @param array<mixed, mixed> $context
	 */
	public function extendContext(array $context): self;

	/**
	 * @return array<mixed, mixed>|null
	 */
	public function getContext(): array|null;
}
