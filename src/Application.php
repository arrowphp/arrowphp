<?php

declare(strict_types=1);

namespace Arrow;

use Arrow\CLI\ServeCommand;
use Arrow\CLI\ClearCacheCommand;
use Arrow\Event\ApplicationCliRunBeforeEvent;
use Arrow\Event\ApplicationWebRunBeforeEvent;
use Arrow\Interface\Middleware;
use Arrow\Exception\RouteNotFound;
use Arrow\Formatter\ResponseFormatter;
use Arrow\Object\Action;
use DI\Container;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use League\Event\EventDispatcher;
use Monolog\Logger;
use Monolog\ErrorHandler;
use Symfony\Component\Console\Application as ConsoleApplication;

class Application {

	public static string $ROOT;
	public static string $VENDOR_DIR;
	public static string $CACHE_DIR;
	public static string $LOG_DIR;

	private readonly EventDispatcher $eventDispatcher;

	/** @var array<string, true> */
	protected array $cliCommands = [];

	public function __construct(
		private readonly Container $container,
		private readonly Router $router,
	) {
		$this->eventDispatcher = new EventDispatcher();
	}

	public function handleWeb(ServerRequest $request): Response {
		ErrorHandler::register($this->container->get(Logger::class));

		$this->eventDispatcher->dispatch(new ApplicationWebRunBeforeEvent($this->container));

		try {
			$routeResult = $this->router->execute($request);

			$next = function (Request $request) use ($routeResult): Action {
				$method = $routeResult->callable->method;
				$controller = $this->container->get($routeResult->callable->serviceId);
				$action = $this->container->call([$controller, $method], $routeResult->data);

				if (!($action instanceof Action)) {
					throw new Exception("Route result is not an " . Action::class);
				}

				return $action;
			};

			// TODO test this works
			// TODO test execution order matches definition order
			foreach (array_reverse($routeResult->middlewares) as $middlewareServiceId) {
				/** @var Middleware */
				$middleware = $this->container->get($middlewareServiceId);

				$inner = $next;
				$next = function (Request $request) use ($middleware, $inner): Action {
					return $middleware->run($inner, $request);
				};
			}

			$action = $next($request);

			return $action->toResponse(new ResponseFormatter());
		} catch (RouteNotFound $e) {
			$logger = $this->container->get(Logger::class);
			$logger->error($e->getMessage(), $e->jsonSerialize());

			// ob_start();
			// echo "<pre>";
			// echo "Error! {$e->getMessage()}\n";
			// echo htmlentities(var_export($e, true));
			// echo "</pre>";
			// $body = ob_get_clean();

			// if ($body === false) {
			// 	$body = '';
			// }

			$response = new Response(500);
		}

		// TODO handle request
		return $response;
	}

	public function runWeb(): void {
		$requestBody = fopen('php://input', 'r');

		if ($requestBody === false) {
			throw new Exception('Unable to open resource `php://input`');
		}

		$request = new ServerRequest(
			$_SERVER['REQUEST_METHOD'],
			$_SERVER['REQUEST_URI'],
			$this->getAllHeaders(),
			$requestBody,
		);

		$response = $this->handleWeb($request);

		foreach ($response->getHeaders() as $header => $values) {
			foreach ($values as $value) {
				header("{$header}: {$value}", true);
			}
		}

		header('Status: ' . $response->getStatusCode(), true, $response->getStatusCode());

		$body = $response->getBody();
		$body->rewind();

		header('Content-Length: ' . $body->getSize());

		echo $body->getContents();
		$body->close();

		flush();
	}

	/** @return array<string, mixed> */
	private function getAllHeaders(): array {
		$headers = [];
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) === 'HTTP_') {
				$name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
				$headers[$name] = $value;
			} elseif ($name === "CONTENT_TYPE") {
				$headers["Content-Type"] = $value;
			} elseif ($name === "CONTENT_LENGTH") {
				$headers["Content-Length"] = $value;
			}
		}

		return $headers;
	}

	public function runCli(): void {
		// ErrorHandler::register($this->container->get(Logger::class));

		$name = join("\n", [
			"                                      _ _  ",
			"   __ _ _ __ _ __ _____      __   ___| (_) ",
			"  / _` | '__| '__/ _ \ \ /\ / /  / __| | | ",
			" | (_| | |  | | | (_) \ V  V /  | (__| | | ",
			"  \__,_|_|  |_|  \___/ \_/\_/    \___|_|_| ",
			"                                           ",
			"    arrow",
		]);

		$console = new ConsoleApplication($name, '1.0');

		$commands = [
			new ServeCommand(),
			new ClearCacheCommand(),
		];
		foreach ($this->cliCommands as $className => $_) {
			$commands[] = $this->container->make($className);
		}

		$this->eventDispatcher->dispatch(new ApplicationCliRunBeforeEvent($commands));

		$console->addCommands($commands);

		$console->run();
	}
}
