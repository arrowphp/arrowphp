<?php

declare(strict_types=1);

namespace Arrow;

use Arrow\Builder\ModulesBuilderOptions;
use Arrow\Builder\RouterBuilderOptions;
use Arrow\Builder\ServicesBuilderOptions;

class ApplicationBuilderOptions {

	/**
	 * @param (callable(ServicesBuilderOptions $options): void)[] $serviceHandlers
	 * @param (callable(RouterBuilderOptions $options): void)[] $routerHandlers
	 * @param (callable(ModulesBuilderOptions $options): void)[] $moduleHandlers
	 */
	public function __construct(
		// @phpstan-ignore property.onlyWritten
		private array &$serviceHandlers,
		// @phpstan-ignore property.onlyWritten
		private array &$routerHandlers,
		// @phpstan-ignore property.onlyWritten
		private array &$moduleHandlers,
	) {
	}

	/**
	 * @property callable(RouterBuilderOptions $options): void $handler
	 */
	public function router(callable $handler): void {
		$this->routerHandlers[] = $handler;
	}

	/**
	 * @property callable(ServicesBuilderOptions $options): void $handler
	 */
	public function services(callable $handler): void {
		$this->serviceHandlers[] = $handler;
	}

	/**
	 * @property callable(ModulesBuilderOptions $options): void $handler
	 */
	public function modules(callable $handler): void {
		$this->moduleHandlers[] = $handler;
	}

}
