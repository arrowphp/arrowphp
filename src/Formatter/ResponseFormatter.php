<?php

declare(strict_types=1);

namespace Arrow\Formatter;

use Arrow\Object\Action;
use Arrow\Interface\Formatter;
use GuzzleHttp\Psr7\Response;

/**
 * @implements Formatter<Action, Response>
 */
class ResponseFormatter implements Formatter {

	/**
	 * @param Action $action
	 * @return Response
	 */
	public function format(mixed $action): Response {
		assert($action instanceof Action);

		$headers = array_merge($action->headers, [
			'Content-Type' => ["application/json"],
		]);

		return new Response(
			$action->status,
			$headers,
			$this->formatData($action->data),
			"1.1",
			$action->error,
		);
	}

	protected function formatData(mixed $data): mixed {
		return "<pre>".print_r($data, true)."</pre>";
	}

}
