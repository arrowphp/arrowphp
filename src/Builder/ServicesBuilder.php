<?php

declare(strict_types=1);

namespace Arrow\Builder;

use Arrow\DI\DefinitionBasic;

class ServicesBuilder {

	public readonly ServicesBuilderOptions $Options;
	private readonly DefinitionBasic $definitionSource;

	public function __construct() {
		$this->definitionSource = new DefinitionBasic();
		$this->Options = new ServicesBuilderOptions(
			definitionSource: $this->definitionSource,
		);
	}

	public function build(): DefinitionBasic {
		return $this->definitionSource;
	}

}
