<?php

declare(strict_types=1);

namespace Arrow\Builder;

use Arrow\Interface\Middleware;
use Arrow\Object\RouteEndpoint;
use Arrow\Object\RouteGroup;
use Arrow\Object\RouteObject;
use Arrow\Object\RoutePath;
use Arrow\Utils\UniqueId;
use Arrow\Router;

class RouterBuilder {

	public readonly RouterBuilderOptions $Options;
	/** @var array<string, class-string<Middleware>[]> */
	private array $middlewares;
	/** @var RouteObject[] */
	private array $routeList;

	private readonly UniqueId $uniqueId;

	public function __construct() {
		$this->uniqueId = new UniqueId('router');

		$this->middlewares = [];
		$this->routeList = [];
		$this->Options = new RouterBuilderOptions(
			basePath: '/',
			middlewares: $this->middlewares,
			routeList: $this->routeList,
		);
	}

	/**
	 * @return Router
	 */
	public function build(): Router {
		[
			'methods' => $methods,
			'pathsGrouped' => $pathsGrouped,
			'endpoints' => $endpoints,
		] = $this->compileRoutes($this->routeList);

		$pathsGroupedOptimized = $this->optimizePathsGrouped($methods, $pathsGrouped);

		$paths = $this->compileRoutePaths($methods, $pathsGroupedOptimized);

		// TODO validate endpoint serviceId is valid
		// TODO validate middleware serviceId is valid, and is Middleware

		return new Router(
			methods: $methods,
			paths: $paths,
			endpoints: $endpoints,
			middlewares: $this->middlewares,
		);
	}

	/**
	 * @param RouteObject[] $routeList
	 * @return array{methods: array<string, string>, pathsGrouped: array<string, array<string, RouteGroup>>, endpoints: array<string, RouteEndpoint>}
	 */
	private function compileRoutes(array $routeList): array {
		/** @var array<string, string> */
		$methods = [];
		/** @var array<string, array<string, RouteGroup>> */
		$pathsGrouped = [];
		/** @var array<string, RouteEndpoint> */
		$endpoints = [];

		foreach ($routeList as $routeObject) {
			$route = $routeObject->route;
			$method = $routeObject->method;

			$fromId = ($this->uniqueId)($method, 'METHOD');
			$methods[$method] = $fromId;

			// Compile Callable
			$routeRegex = $this->compileRouteRegex($route, true);
			$regex = '/^' . $routeRegex . '$/';

			$toId = ($this->uniqueId)($route, $method);
			$endpoints[$toId] = new RouteEndpoint(
				regex: $regex,
				callable: $routeObject->callable,
			);

			// Compile Path Parts Grouped
			if ($route === '/') {
				$routeParts = ['/'];
			} else {
				$routeParts = explode('/', $route);
			}

			for ($i = 0; $i < count($routeParts); $i++) {
				$part = $routeParts[$i];
				$subPath = join('/', array_slice($routeParts, 0, $i + 1));

				$toId = ($this->uniqueId)($subPath, $method);

				if (!isset($pathsGrouped[$fromId])) {
					$pathsGrouped[$fromId] = [];
				}
				if (!isset($pathsGrouped[$fromId][$part])) {
					$index = count($pathsGrouped[$fromId]);
					$pathsGrouped[$fromId][$part] = new RouteGroup(
						toId: $toId,
						part: $part,
						index: $index,
					);
				}
				$fromId = $toId;
			};
		}

		return [
			'methods' => $methods,
			'endpoints' => $endpoints,
			'pathsGrouped' => $pathsGrouped,
		];
	}

	/**
	 * @param array<string, string> $methods
	 * @param array<string, array<string, RouteGroup>> $pathsGrouped
	 * @return array<string, array<string, RouteGroup>>
	 */
	private function optimizePathsGrouped(array $methods, array $pathsGrouped): array {
		$fromIdsToCheck = array_values($methods);
		while (($fromId = array_shift($fromIdsToCheck)) !== null) {
			$values = $pathsGrouped[$fromId];

			$parts = array_keys($values);
			while (($part = array_shift($parts)) !== null) {
				$routeGroup = $values[$part];
				$toId = $routeGroup->toId;

				if (!isset($pathsGrouped[$toId])) {
					// We have reached the end of our route.
					continue;
				}

				$toValues = $pathsGrouped[$toId];
				if (count($toValues) > 1) {
					// The next step need processing, queue for next.
					array_unshift($fromIdsToCheck, $toId);
					continue;
				}

				$nextPart = array_keys($toValues)[0];
				$nextRouteGroup = $toValues[$nextPart];

				$newPart = $part . '/' . $nextPart;

				$values[$newPart] = new RouteGroup(
					toId: $nextRouteGroup->toId,
					part: $newPart,
					index: $routeGroup->index,
					partCount: $routeGroup->partCount + $nextRouteGroup->partCount,
				);

				unset($pathsGrouped[$toId]);
				unset($values[$part]);

				array_unshift($parts, $newPart);
			}

			$pathsGrouped[$fromId] = $values;
		}

		return $pathsGrouped;
	}

	/**
	 * @param array<string, string> $methods
	 * @param array<string, array<string, RouteGroup>> $pathsGrouped
	 * @return array<string, RoutePath>
	 */
	private function compileRoutePaths(array $methods, array $pathsGrouped): array {
		/** @var array<string, RoutePath> */
		$paths = [];
		/** @var array<string, int> */
		$fromIdMapOffset = [];

		$fromIdsToProcess = array_values($methods);
		while (($fromId = array_shift($fromIdsToProcess)) !== null) {
			/** @var array<int, string> */
			$regexParts = [];
			foreach ($pathsGrouped[$fromId] as $routeGroup) {
				$toId = $routeGroup->toId;
				$part = $routeGroup->part;
				$index = $routeGroup->index;
				$partCount = $routeGroup->partCount;

				$partRegex = $this->compileRouteRegex($part, false);
				$regexParts[$index] = '(?<' . $toId . '>' . $partRegex . ')';

				if (isset($pathsGrouped[$toId])) {
					$fromOffset = isset($fromIdMapOffset[$fromId])
						? $fromIdMapOffset[$fromId]
						: 0;

					$fromIdMapOffset[$toId] = $fromOffset + $partCount;

					array_unshift($fromIdsToProcess, $toId);
				}
			}

			$offset = isset($fromIdMapOffset[$fromId])
				? $fromIdMapOffset[$fromId]
				: 0;

			ksort($regexParts);
			$regex = '/^(' . join('|', $regexParts) . ')/';

			$paths[$fromId] = new RoutePath($offset, $regex);
		}

		return $paths;
	}

	private function compileRouteRegex(string $route, bool $includeDataName): string {
		if ($route === '/') {
			return '\/\/';
		}

		$routeParts = explode('/', ltrim($route, '/'));

		$splitRegex = '/(\{[^}]+?\})/';
		$dataMatchRegex = '/^\{([^}\/]*?)\}$/';
		foreach ($routeParts as &$part) {
			$partsSplit = preg_split($splitRegex, $part, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

			if ($partsSplit === false) {
				throw new \Arrow\Exception('Failed to preg_split', [
					'regex' => $splitRegex,
					'part' => $part,
				]);
			}

			foreach ($partsSplit as &$partPart) {
				if (preg_match($dataMatchRegex, $partPart, $matches) === 1) {
					if ($includeDataName) {
						$name = preg_quote($matches[1]);
						$partPart = '(?<' . $name . '>[^\/]*?)';
					} else {
						$partPart = '([^\/]*?)';
					}
				} else {
					$partPart = preg_quote($partPart, '/');
				}
			}

			$part = join('', $partsSplit);
		}

		return '\/' . join('\/', $routeParts) . '\/';
	}

}
