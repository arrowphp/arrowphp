<?php

declare(strict_types=1);

namespace Arrow\DI;

use DI\Definition\Definition;
use DI\Definition\Source\DefinitionNormalizer;
use DI\Definition\Source\DefinitionSource;
use DI\Definition\Source\MutableDefinitionSource;
use DI\Definition\Source\NoAutowiring;

class DefinitionBasic implements DefinitionSource, MutableDefinitionSource {

	/**
	 * DI definitions in a PHP array.
	 * @var array<string, mixed>
	 */
	private array $definitions;

	private DefinitionNormalizer $normalizer;

	/**
	 * @param array<string, mixed> $definitions
	 */
	public function __construct(array $definitions = []) {
		// @phpstan-ignore-next-line
		if (isset($definitions[0])) {
			throw new \Exception('The PHP-DI definition is not indexed by an entry name in the definition array');
		}

		$this->definitions = $definitions;

		$this->normalizer = new DefinitionNormalizer(new NoAutowiring);
	}

	public function addDefinition(Definition $definition) : void {
		$this->definitions[$definition->getName()] = $definition;
	}

	public function getDefinition(string $name) : Definition|null {
		// Look for the definition by name
		if (!array_key_exists($name, $this->definitions)) {
			return null;
		}

		$definition = $this->definitions[$name];

		return $this->normalizer->normalizeRootDefinition($definition, $name);
	}

	public function getDefinitions() : array {
		return $this->definitions;
	}
}
