<?php

declare(strict_types=1);

namespace Arrow\Interface;

use Arrow\ApplicationBuilderOptions;

interface Module {

	public function register(ApplicationBuilderOptions $options): void;

}
