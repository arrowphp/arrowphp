<?php

declare(strict_types=1);

namespace Arrow\Exception;

class RouteNotFound extends \Arrow\Exception {
	public function __construct(
		string $route,
		array $data = [],
	) {
		parent::__construct(
			message: "Route Not Found",
			context: [
				'route' => $route,
				'data' => $data,
			]
		);
	}
}
