<?php

declare(strict_types=1);

namespace Arrow\Exception;

use Arrow\Throwable;

class RouteError extends \Arrow\Exception {
	/**
	 * @param array<string, mixed> $data
	 */
	public function __construct(
		string $route,
		string $reason,
		array $data = [],
		?Throwable $previous = null,
	) {
		parent::__construct(
			message: "Error processing route: $reason",
			context: [
				'route' => $route,
				'reason' => $reason,
				'data' => $data,
			],
			previous: $previous,
		);
	}
}
