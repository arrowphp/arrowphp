<?php

declare(strict_types=1);

namespace Arrow\Object;

class RouteCallable {
	public function __construct(
		public readonly string $serviceId,
		public readonly ?string $method = null,
	) {}
}
