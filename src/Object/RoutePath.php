<?php

declare(strict_types=1);

namespace Arrow\Object;

class RoutePath {
	public function __construct(
		public readonly int $offset,
		public readonly string $regex,
	) {}
}
