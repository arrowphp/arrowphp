<?php

declare(strict_types=1);

namespace Arrow\Object;

class RouteGroup {
	public function __construct(
		public readonly string $toId,
		public readonly string $part,
		public readonly int $index,
		public int $partCount = 1,
	){}
}
