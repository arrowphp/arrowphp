<?php

declare(strict_types=1);

namespace Arrow\Object;

use Arrow\Interface\Middleware;

class RouteResult {
	/**
	 * @param array<string, mixed> $data
	 * @param class-string<Middleware>[] $middlewares
	 */
	public function __construct(
		public readonly RouteCallable $callable,
		public readonly array $data,
		public readonly array $middlewares,
	) {}
}
