<?php

namespace Arrow\Utils;

class UniqueId {
	private static string $Alphabet = 'hDFmXHtZyTQGsJgcBzaYSuokvPALwipneKlrUdCWVqENxOMfbjRI';
	private static int $AlphabetSize = 52;

	/** @var array<string, string> */
	private static array $UniqueIdCache = [];
	/** @var array<string, int> */
	private static array $Count = [];

	public function __construct(
		private string $namespace
	) {}

	public function __invoke(string $value, string $prefix = '0'): string {
		return UniqueId::Get($value, $prefix, $this->namespace);
	}

	public static function Get(string $value, string $prefix = '0', string $namespace = '__global'): string {
		$hash = sha1("{$namespace}-{$prefix}-{$value}");

		if (isset(UniqueId::$UniqueIdCache[$hash])) {
			return UniqueId::$UniqueIdCache[$hash];
		}

		if (!isset(UniqueId::$Count[$namespace])) {
			UniqueId::$Count[$namespace] = 0;
		}

		$int = UniqueId::$Count[$namespace]++;

		return UniqueId::$UniqueIdCache[$hash] = UniqueId::ConvertDecimalToId($int);
	}

	private static function ConvertDecimalToId(int $int): string {
		$id = UniqueId::$Alphabet[$int % UniqueId::$AlphabetSize];

		while (($int /= UniqueId::$AlphabetSize) >= 1) {
			$id .= UniqueId::$Alphabet[$int % UniqueId::$AlphabetSize];
		}

		return $id;
	}
}
