<?php

namespace Arrow\CLI;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Arrow\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearCacheCommand extends Command {

	protected function configure() {
		$this->setName('warm');
		$this->setDescription('Clear the application cache created when running in Production');
		$this->setHelp(<<<'EOF'
The <info>%command.name%</info> command clears the cache to allow for new cache to be created when running in production:

  <info>%command.full_name%</info>

This step delete the whole <cache> directory.
EOF);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		if (is_dir(Application::$CACHE_DIR)) {
			// Clear existing cache
			$files = new RecursiveIteratorIterator(
				new RecursiveDirectoryIterator(Application::$CACHE_DIR, RecursiveDirectoryIterator::SKIP_DOTS),
				RecursiveIteratorIterator::CHILD_FIRST
			);

			foreach ($files as $fileinfo) {
				if ($fileinfo->isDir()) {
					rmdir($fileinfo->getRealPath());
				} else {
					unlink($fileinfo->getRealPath());
				}
			}
		}

		return Command::SUCCESS;
	}
}
