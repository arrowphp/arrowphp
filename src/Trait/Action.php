<?php

declare(strict_types=1);

namespace Arrow\Trait;

use Arrow\Object\Action as ObjectAction;

trait Action {

	protected function action(
		int $status,
		mixed $data = null,
		mixed $error = null,
		array $headers = [],
	): ObjectAction {
		return new ObjectAction(
			$status,
			$data,
			$error,
			$headers,
		);
	}

	protected function ok(
		mixed $data,
	): ObjectAction {
		return $this->action(200, $data);
	}

	protected function error(string $reason = null, int $status = 500) {
		return $this->action($status, null, $reason);
	}

	protected function notFound(string $reason = null) {
		return $this->error($reason, 404);
	}

}
