<?php

declare(strict_types=1);

namespace Arrow;

use JsonSerializable;
use Monolog\Utils;
use Monolog\Formatter\NormalizerFormatter;

/**
 * @codeCoverageIgnore
 */
class Exception extends \Exception implements Throwable, JsonSerializable {

	/**
	 * @var array<mixed, mixed>|null
	 */
	private mixed $context = null;

	/**
	 * @param array<string, mixed> $context
	 */
	public function __construct(string $message = "", array $context = [], int $code = 0, Throwable $previous = null) {
		parent::__construct($message, $code, $previous);
		$this->context = $context;
	}


	/**
	 * @param array<mixed, mixed> $context
	 */
	public function extendContext(array $context): self {
		$this->context = array_merge($this->context ?? [], $context);
		return $this;
	}

	/**
	 * @return array<mixed, mixed>|null
	 */
	public function getContext(): array|null {
		return $this->context;
	}

	/** @return array<string, mixed> */
	public function jsonSerialize(): array {
		$data = [
			'class' => Utils::getClass($this),
			'message' => $this->getMessage(),
			'code' => $this->getCode(),
			'file' => $this->getFile().':'.$this->getLine(),
		];

		if (!is_null($this->getContext())) {
			$data['context'] = $this->getContext();
		}

		$trace = $this->getTrace();
		foreach ($trace as $frame) {
			if (isset($frame['file'], $frame['line'])) {
				$data['trace'][] = $frame['file'].':'.$frame['line'];
			}
		}

		if (($previous = $this->getPrevious()) instanceof \Throwable) {
			$normalizeFormatter = new NormalizerFormatter();
			$data['previous'] = $normalizeFormatter->normalize($previous, 1);
		}

		return $data;
	}
}
