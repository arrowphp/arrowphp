# Changelog

All notable changes to `arrowphp/arrow` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [0.2.20191024][0.2.20191024] - 2019-10-24

## Added
- Application now supports returning any data type from a route callable
- Application is now easier to run
  - No longeg requires a response to be supplied
  - Passing in a request is now optional; if not specified, it pulls in the global requests data
- ApplicationStrategy now automatically converts your response into a Json response if request Content-Type = "application/json"
- ApplicationStrategy tests have been updateed
- New Factory to centralise the creation of objects
- New Events 
  - ApplicationCreateRequestEvent - Use this event to return your own \Psr\Http\Message\ServerRequestInterface 
  - ApplicationCreateResponseEvent  - Use this event to return your own \Psr\Http\Message\ResponseInterface 
  - ResponseFormatJsonEvent - Use this event to add your own formatting to the Json response
  - RoutePostCallEvent - Use this event to handle the route callable response before the ApplicationStrategy
- Added config "ErrorLogFile" to allow specifying log file location. It can be set to null to disable the default log handler
- Added Build init files from template; they are copied if they are not already present
  - Files: .gitignore, http-server.php, config/config.php, config/config.middleware.php, http/index.php
- Updated to phpunit/phpunit v8, and phpunit/php-code-coverage ^7, and squizlabs/php_codesniffer ^3

## Fixed
- Refactored Build process and improved logging
- Fixed \Arrow\Exception typo and default value

## Removed
- Removed Util/ItemCollection and Util/UniqueItemCollection

## [0.1.20190206][0.1.20190206] - 2019-02-06

## Added
- New composer `run-coverage` command to generate the coverates reports.

## Fixed
- Documentation in README.
- Code coverage report to skip untestable code

## [0.1.20190203][0.1.20190203] - 2019-02-03

## Added
- New simplifed Config implementation.
- New Package functionality. This allows the core framework to remain lean and for the developer to only include what they require.
  - Moved CLI functionality in it's own package.
  - Moved Database functionality into it's own package.
- New Build Tool which pulls all config files into one, and caches them.
- New Event
  - ApplicationHandleExceptionEvent. This is emitted before an exception is handled.
- Added Arrow\Throwable and Arrow\Exception, which support additional context data for error logging.
- Added static Helper class with method `var_export_pretty`
- Added `arrowphp/cli` as a suggested package to composer.json.

## Fixed
- Refactored the Application boot method to handle errors.
- When loading modules, the config `Module.ClassNamePattern` now supports `{Namespace}` and `{Name}`. Previously it was only `<Name>`.
- Fixed tests to match updates.
- Fixed typo in ApplicationTest.
- Simplified CI script.

### Removed
- Removed DisplayErrors config option and logic.
- Removed throwException option on the Application.

## [0.1.20180615][0.1.20180615] - 2018-06-15

### Added
- Introduced the Event RoutePreCallEvent. The ApplicationStrategy emits this event before calling the Route.

### Fixed
- Locked down version for squizlabs/php_codesniffer.
- All PDO connections now throw exceptions on error.
- Fixed the badges and description in the README.md
- Ran `composer update` to update modules

## 0.0.20180610 - 2018-06-10

*Setup working project.*

[0.1.20190206]: https://gitlab.com/arrowphp/arrow/compare/v0.1.20190203...v0.1.20190206
[0.1.20190203]: https://gitlab.com/arrowphp/arrow/compare/v0.1.20180615...v0.1.20190203
[0.1.20180615]: https://gitlab.com/arrowphp/arrow/compare/v0.0.20180610...v0.1.20180615

<!-- 

Template - see: https://keepachangelog.com/en/1.0.0/

## [0.0.20180000] - 2018-00-00

### Added
- Nothing

### Deprecated
- Nothing

### Fixed
- Nothing

### Removed
- Nothing

### Security
- Nothing

-->
