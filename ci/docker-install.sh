#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq \
  && apt-get install -yqq zip git

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
