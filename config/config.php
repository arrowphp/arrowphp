<?php declare(strict_types=1);

return [
    // Enables verbose logging
    'Debug' => false,
    'Path' => [
        // Absolute path to the project root
        'Base' => null,
        // Absolute path to the arrow root
        'Self' => null,
    ],
    // Application namespace
    'Namespace' => 'App',
    // Relative file path to error log - set to null to disable
    'ErrorLogFile' => '/logs/app.log',

    'Module' => [
        // Class name of the module class given the module {Name} and {Namespace}
        'ClassNamePattern' => '\{Namespace}\{Name}\{Name}Module',
    ],

    'Route' => [
        // Controller and action for Route not found
        'NotFound' => ['module.notfound.controller', 'notFound'],
        // Controller and action for Route found, but not allow / permitted
        'NotAllowed' => ['module.notfound.controller', 'notAllowed'],
    ],

	'Middleware' => [
		\Arrow\Middleware\ModuleLoader::class,
	],
];
