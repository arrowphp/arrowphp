<?php

declare(strict_types=1);

namespace TestApp\ModuleTest;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class ModuleTestController {

	/**
	 * @return mixed
	 */
	public function returnResponse() {
		$response = new Response();

		$response->getBody()
			->write('returnResponse');

		return $response;
	}

	/**
	 * @return mixed
	 */
	public function returnArray() {
		return ['returnArray'];
	}
}
