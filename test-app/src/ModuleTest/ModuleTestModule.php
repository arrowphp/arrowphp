<?php

declare(strict_types=1);

namespace TestApp\ModuleTest;

use Arrow\Config;
use Arrow\ModuleInterface;
use League\Container\Container;
use Arrow;

class ModuleTestModule implements ModuleInterface {

	public function registerRoutes(Router $router): void {
	}

	public function registerServices(Container $container, Config $config): void {
		$container->add('module.moduletest.controller', ModuleTestController::class);
	}
}
