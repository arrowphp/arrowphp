<?php

declare(strict_types=1);

namespace TestApp\AltModuleTest;

use Arrow\Config;
use Arrow\ModuleInterface;
use League\Container\Container;
use Arrow;

class ModuleTest implements ModuleInterface {

	public function registerRoutes(Router $router): void {
	}

	public function registerServices(Container $container, Config $config): void {
	}
}
