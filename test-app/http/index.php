<?php declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

/** @var \TestApp\Application */
$app = require_once '../application.php';
$app->runWeb();
